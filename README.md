This repository is an adaptation of the [DeepSVDD](https://github.com/lukasruff/Deep-SVDD-PyTorch.git) repository:
instead of relying on an hyper-sphere for anomaly detection, we rather adapt an unsupervised classifier risk
on top of a final neural classification layer.

Apart from the final classification layer and training objective, this repository follows the same experimental
setup than the original Deep-SVDD repository.

Please refer to the [DeepSVDD](https://github.com/lukasruff/Deep-SVDD-PyTorch.git) repository for a description of
the baseline method.
To reproduce the results on MNIST, please install pytorch and run the script "./run.sh"

-----

Although this code is opensource and is hosted in an anonymous repository, I (the owner of the repository) reserve
all rights on this code for now.

