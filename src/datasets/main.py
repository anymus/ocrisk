from .mnist import MNIST_Dataset
from .mnist import MNIST_Dataset_dev
from .mnist import MNIST_Dataset_original
from .cifar10 import CIFAR10_Dataset


def load_dataset(dataset_name, data_path, normal_class):
    """Loads the dataset."""

    implemented_datasets = ('mnist', 'cifar10', 'mnist_original', 'mnist_dev')
    assert dataset_name in implemented_datasets

    dataset = None

    if dataset_name == 'mnist_original':
        dataset = MNIST_Dataset_original(root=data_path, normal_class=normal_class)

    if dataset_name == 'mnist':
        dataset = MNIST_Dataset(root=data_path, normal_class=normal_class)

    if dataset_name == 'mnist_dev':
        dataset = MNIST_Dataset_dev(root=data_path, normal_class=normal_class)

    if dataset_name == 'cifar10':
        dataset = CIFAR10_Dataset(root=data_path, normal_class=normal_class)

    return dataset
