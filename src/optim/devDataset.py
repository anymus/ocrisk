from base.torchvision_dataset import TorchvisionDataset
from torch.utils.data import TensorDataset

class DevDataset(TorchvisionDataset):

    def __init__(self, xtensor, ytensor):
        super().__init__(".")

        ds = TensorDataset(xtensor,ytensor)

        self.train_set = ds
        self.test_set = ds


