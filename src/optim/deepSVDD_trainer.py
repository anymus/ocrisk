from base.base_trainer import BaseTrainer
from base.base_dataset import BaseADDataset
from base.torchvision_dataset import TorchvisionDataset
from torch.utils.data import TensorDataset
from base.base_net import BaseNet
from torch.utils.data.dataloader import DataLoader
from sklearn.metrics import roc_auc_score
from optim.unsuprisk import UnsupRisk

from sklearn.mixture import GaussianMixture

import time
import torch
import torch.nn as nn
import torch.optim as optim
import numpy as np

class DevDataset(TorchvisionDataset):

    def __init__(self, xtensor, ytensor):
        super().__init__(".")

        idx = torch.LongTensor(np.arange(xtensor.size(0)))
        ds = TensorDataset(xtensor,ytensor,idx)

        self.train_set = ds
        self.test_set = ds

class UnsupriskTrainer(BaseTrainer):

    def __init__(self, embedsize, objective, prior0: float, optimizer_name: str = 'adam', lr: float = 0.001, n_epochs: int = 150,
                 lr_milestones: tuple = (), batch_size: int = 128, weight_decay: float = 1e-6, device: str = 'cuda',
                 n_jobs_dataloader: int = 0):
        super().__init__(optimizer_name, lr, n_epochs, lr_milestones, batch_size, weight_decay, device, n_jobs_dataloader)

        assert objective in ('exact', 'approx'), "Objective must be either 'exact' or 'approx'."
        self.objective = objective

        self.device = device
        self.lastlay = nn.Linear(1+embedsize,1).to(device)
        self.prior0 = prior0

        # init last linear layer so that it returns the AE score
        self.lastlay.weight.data.fill_(0.0)
        self.lastlay.bias.data.fill_(0.0)
        tmpidx = torch.tensor([0]).to(device)
        self.lastlay.weight.data.index_fill_(1,tmpidx,1.)

        # Results
        self.train_time = None
        self.test_auc = None
        self.test_time = None
        self.test_scores = None

    def train(self, dataset: BaseADDataset, net: BaseNet, aemodel, originalSVDD):
        self.originalSVDD = originalSVDD
        train_loader, _ = dataset.loaders(batch_size=self.batch_size, num_workers=self.n_jobs_dataloader)

        # Set device for network
        net = net.to(self.device)

        # Training
        optimizerRisk = optim.Adam(self.lastlay.parameters(), lr=self.lr, weight_decay=self.weight_decay)

        print('Starting origin training...')
        start_time = time.time()
        net.eval()
        lossfct = UnsupRisk(self.prior0,self.device)
        SVDDc = torch.Tensor(self.originalSVDD.c).to(self.device)
        testaucs = []
        devaucs = []
        for epoch in range(self.n_epochs):
            loss_epoch = 0.0
            n_batches = 0
            epoch_start_time = time.time()
            idx_label_score = []
            for data in train_loader:
                inputs, labels, idx = data
                inputs = inputs.to(self.device)
                optimizerRisk.zero_grad()

                outputs = net(inputs)
                dist = torch.sum((outputs - SVDDc) ** 2, dim=1)
                dist = dist.detach()
                svddscores = dist - self.originalSVDD.R ** 2
                svddscores = svddscores.view(-1,1)
                ii = torch.cat((svddscores,outputs),dim=1)

                scores = self.lastlay(ii)
                loss = lossfct(scores)
                if not (float('-inf') < float(loss.item()) < float('inf')):
                    # nan or inf
                    continue

                loss.backward()
                optimizerRisk.step()

                loss_epoch += loss.item()
                n_batches += 1

            # log epoch statistics
            epoch_train_time = time.time() - epoch_start_time
            if n_batches==0: n_batches=1
            self.test(dataset, net, aemodel)
            testaucs.append(self.test_auc)
            testres = self.test_auc
            if self.dev!=None:
                self.test(self.dev, net, aemodel)
                devaucs.append(self.test_auc)
                devres = self.test_auc
            else: devres = 0.
            print('  Epoch {}/{}\t Time: {:.3f}\t Loss: {:.8f} Dev {:.8f} Test {:.8f}' .format(epoch + 1, self.n_epochs, epoch_train_time, loss_epoch / n_batches, devres, testres))

        self.train_time = time.time() - start_time
        print('Training time: %.3f' % self.train_time)

        return testaucs

    def test(self, dataset: BaseADDataset, net: BaseNet, aemodel):

        # Set device for network
        net = net.to(self.device)

        # Get test data loader
        _, test_loader = dataset.loaders(batch_size=self.batch_size, num_workers=self.n_jobs_dataloader)

        # Testing
        print('Starting testing...')
        start_time = time.time()
        idx_label_score = []
        net.eval()
        lossfct = UnsupRisk(self.prior0,self.device)
        with torch.no_grad():
            SVDDc = torch.Tensor(self.originalSVDD.c).to(self.device)
            for data in test_loader:
                inputs, labels, idx = data
                inputs = inputs.to(self.device)
                outputs = net(inputs)
                dist = torch.sum((outputs - SVDDc) ** 2, dim=1)
                # TODO: add an option for approx
                svddscores = dist - self.originalSVDD.R ** 2
                svddscores = svddscores.view(-1,1)
                ii = torch.cat((svddscores,outputs),dim=1)
                scores = self.lastlay(ii)

                # Save triples of (idx, label, score) in a list
                idx_label_score += list(zip(idx.cpu().data.numpy().tolist(),
                                            labels.cpu().data.numpy().tolist(),
                                            scores.cpu().data.numpy().tolist()))
                risk = lossfct(scores).item()
                # print("testunsuprisk "+str(risk))

        self.test_time = time.time() - start_time
        print('Testing time: %.3f' % self.test_time)

        self.test_scores = idx_label_score

        # Compute AUC
        _, labels, scores = zip(*idx_label_score)
        labels = np.array(labels)
        scores = np.array(scores)

        try:
            self.test_auc = roc_auc_score(labels, scores)
        except:
            print("error calcul auc")
            self.test_auc = 0.
        print('Test set AUC: {:.2f}%'.format(100. * self.test_auc))

        print('Finished testing.')

